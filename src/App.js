import React from 'react';
import 'antd/dist/antd.css';
import { Alert } from 'antd';
import './App.css';
import './index.css';

const scaleNames = {
    c: 'Celsius',
    f: 'Fahrenheit'
}

/**
 T(°F) = T(°C) x 9/5 + 32
 T(°C) = (T(°F) - 32) x 5/9
**/
function toCelsius (fahrenheit) {
    return (fahrenheit - 32) * 5 / 9
}

function toFahrenheit (celsius) {
    return (celsius * 9 / 5) + 32
}

function BoilingVerdict({celsius}) { // Fonction BoilingVerdict qui prend en paramètres des propriétés (ici seulement celcius)
    if (celsius >= 100) { // si la proprité celsius de la fonction est plus grande ou égale à 100
        return <Alert message="L'eau bout" type = "success" />
    } return <Alert message="L'eau ne bout pas" type = "info" />
}

class TemperatureInput extends React.Component {
    constructor(props){
        super(props);
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange (e) {
        // lorsque tu changes les choses, je veux que =>
        this.props.onTemperatureChange(e.target.value)
    }

    render(){
        const {temperature} = this.props
        const name = 'scale' + this.props.scale
        const scaleName = scaleNames[this.props.scale] //valeur associée à la clé
        return <div className="form-group">
            <label htmlFor={name}>Température (en {scaleName})</label>
            <input type="text" id={name} value={temperature} className="form-control" onChange={this.handleChange}/>
        </div>
    }
}

class App extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            scale: 'c',
            temperature: 20
        }
        this.handleCelsiusChange = this.handleCelsiusChange.bind(this)
        this.handleFahrenheitChange = this.handleFahrenheitChange.bind(this)
    }

    handleCelsiusChange (temperature) {
        this.setState({
            scale: 'c',
            temperature
        })
    }

    handleFahrenheitChange (temperature) {
        this.setState({
            scale: 'f',
            temperature
        })
    }

    render(){
        const {temperature, scale} = this.state
        const celsius = scale === 'c' ? temperature : toCelsius(temperature)
        const fahrenheit = scale === 'f' ? temperature : toFahrenheit(celsius)
        return (
            <div>
                <header>
                    <h1>Convertisseur de degrés celsius et fahrenheit</h1>
                </header>
                <body>
                    <TemperatureInput scale="c" temperature={celsius} onTemperatureChange={this.handleCelsiusChange}/> {/*temperature et handleTemperatureChange sont des propriétés*/}
                    <TemperatureInput scale="f" temperature={fahrenheit} onTemperatureChange={this.handleFahrenheitChange}/>
                    <BoilingVerdict celsius={parseFloat(temperature)}/>{/*parser la chaîne de caractère sous forme de float*/}
                </body>
                <footer>
                </footer>
            </div>
        );
    }
}

export default App;
